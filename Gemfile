source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'rails', '~> 5.2.1'
gem 'puma'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'graphql', '1.7.4'
gem 'sidekiq'
gem 'exception_notification'
gem 'cancan'
gem 'bootstrap-datepicker-rails'
gem 'rails-jquery-autocomplete'
gem 'simple_form'
gem 'awesome_print'
gem 'jbuilder', '~> 2.5'
gem 'redis', '~> 4.0'
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  gem 'sqlite3'
  gem 'rspec-rails'
  gem "factory_bot_rails"
  gem 'pry'
  gem 'byebug'
  gem 'dotenv-rails', '~> 2.1', '>= 2.1.1'
  gem 'rails-controller-testing'
end

group :development do
  gem 'graphiql-rails', '1.4.4'
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'


  gem 'faker'
  gem 'cpf_faker'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'webmock'
  gem 'shoulda-matchers'
  gem 'poltergeist'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :production do
  gem 'unicorn'
  gem 'rails_12factor'
  gem 'newrelic_rpm'
end